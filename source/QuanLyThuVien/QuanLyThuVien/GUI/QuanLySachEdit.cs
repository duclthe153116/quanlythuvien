﻿﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using QuanLyThuVien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.GUI
{



    public partial class QuanLySachEdit : Form
    {

        quanlythuvienContext context;
        public QuanLySachEdit(int maSach)
        {
            InitializeComponent();
            context = new quanlythuvienContext();

            loadData(maSach);

        }

        #region methods
        void loadData(int maSach)
        {
            
            Sach sach = context.Saches.Find(maSach);
            txbETenSach.Text = sach.TenSach;
            lbMaSachValue.Text = $"{maSach}";

            var conf = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", true, true)
               .Build();

            SqlConnection connection = new SqlConnection(conf.GetConnectionString("DBconnection"));

            string query1 = "select TenLoaiSach from LoaiSach";

            SqlCommand command = new SqlCommand(query1, connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            cbELoaiSach.DisplayMember = "TenLoaiSach";
            cbELoaiSach.ValueMember = "TenLoaiSach";
            cbELoaiSach.DataSource = table;
            cbELoaiSach.Text = context.LoaiSaches.Find((sach.MaLoaiSach)).TenLoaiSach;

            string query2 = "select TenTacGia from TacGia";

            SqlCommand command2 = new SqlCommand(query2, connection);
            SqlDataAdapter adapter2 = new SqlDataAdapter(command2);
            DataTable table2 = new DataTable();
            adapter2.Fill(table2);

            cbETenTacGia.DisplayMember = "TenTacGia";
            cbETenTacGia.ValueMember = "TenTacGia";
            cbETenTacGia.DataSource = table2;
            cbETenTacGia.Text = context.TacGia.Find((sach.MaTacGia)).TenTacGia;


            rbEDaTra.Checked = false;
            rbEChuaTra.Checked = false;

            if (sach.TrangThai == true)
            {
                rbEDaTra.Checked = true;
                rbEDaTra.Enabled = false;
                rbEChuaTra.Enabled = false;
            }
            else
            {
                rbEChuaTra.Checked = true;
            }
        }

        #endregion


        private void btnELuu_Click(object sender, EventArgs e)
        {

            if (txbETenSach.Text == "")
            {
                MessageBox.Show("Vui lòng điền tên sách!");
                return;
            }

            int maSach = Convert.ToInt32(lbMaSachValue.Text);
            Sach sach = context.Saches.Find(maSach);

            sach.TenSach = txbETenSach.Text;

            bool daTra = false;
            if (rbEDaTra.Checked == true)
            {
                daTra = true;
            }
            sach.TrangThai = daTra;

            //Insert into database
            int LoaiSachID = -1;
            int TacGiaID = -1;

            var k = from l in context.LoaiSaches
                    where l.TenLoaiSach == cbELoaiSach.Text
                    select l.MaLoaiSach;
            List<int> t = k.ToList();
            LoaiSachID = t[t.Count - 1];

            var m = from l in context.TacGia
                    where l.TenTacGia == cbETenTacGia.Text
                    select l.MaTacGia;
            List<int> n = m.ToList();
            TacGiaID = n[n.Count - 1];

            sach.MaLoaiSach = LoaiSachID;
            sach.MaTacGia = TacGiaID;

            context.Saches.Update(sach);
            context.SaveChanges();

            MessageBox.Show("Lưu thay đổi thành công!");

            QuanLySach f = new QuanLySach();
            this.Hide();
            f.Show();
        }

    }
}