﻿namespace QuanLyThuVien.GUI
{
    partial class QuanLySachEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbETenSach = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnELuu = new System.Windows.Forms.Button();
            this.rbEDaTra = new System.Windows.Forms.RadioButton();
            this.rbEChuaTra = new System.Windows.Forms.RadioButton();
            this.cbETenTacGia = new System.Windows.Forms.ComboBox();
            this.cbELoaiSach = new System.Windows.Forms.ComboBox();
            this.lbMaSach = new System.Windows.Forms.Label();
            this.lbMaSachValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txbETenSach
            // 
            this.txbETenSach.Location = new System.Drawing.Point(336, 72);
            this.txbETenSach.Name = "txbETenSach";
            this.txbETenSach.Size = new System.Drawing.Size(185, 27);
            this.txbETenSach.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(245, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tên Sách";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 323);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Trạng Thái Trả Sách";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tên Tác Giả";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(245, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Loại Sách";
            // 
            // btnELuu
            // 
            this.btnELuu.Location = new System.Drawing.Point(336, 414);
            this.btnELuu.Name = "btnELuu";
            this.btnELuu.Size = new System.Drawing.Size(94, 29);
            this.btnELuu.TabIndex = 9;
            this.btnELuu.Text = "Lưu";
            this.btnELuu.UseVisualStyleBackColor = true;
            this.btnELuu.Click += new System.EventHandler(this.btnELuu_Click);
            // 
            // rbEDaTra
            // 
            this.rbEDaTra.AutoSize = true;
            this.rbEDaTra.Location = new System.Drawing.Point(336, 323);
            this.rbEDaTra.Name = "rbEDaTra";
            this.rbEDaTra.Size = new System.Drawing.Size(71, 24);
            this.rbEDaTra.TabIndex = 10;
            this.rbEDaTra.TabStop = true;
            this.rbEDaTra.Text = "Đã trả";
            this.rbEDaTra.UseVisualStyleBackColor = true;
            // 
            // rbEChuaTra
            // 
            this.rbEChuaTra.AutoSize = true;
            this.rbEChuaTra.Location = new System.Drawing.Point(453, 323);
            this.rbEChuaTra.Name = "rbEChuaTra";
            this.rbEChuaTra.Size = new System.Drawing.Size(86, 24);
            this.rbEChuaTra.TabIndex = 11;
            this.rbEChuaTra.TabStop = true;
            this.rbEChuaTra.Text = "Chưa trả";
            this.rbEChuaTra.UseVisualStyleBackColor = true;
            // 
            // cbETenTacGia
            // 
            this.cbETenTacGia.FormattingEnabled = true;
            this.cbETenTacGia.Location = new System.Drawing.Point(336, 238);
            this.cbETenTacGia.Name = "cbETenTacGia";
            this.cbETenTacGia.Size = new System.Drawing.Size(185, 28);
            this.cbETenTacGia.TabIndex = 62;
            // 
            // cbELoaiSach
            // 
            this.cbELoaiSach.FormattingEnabled = true;
            this.cbELoaiSach.Location = new System.Drawing.Point(336, 156);
            this.cbELoaiSach.Name = "cbELoaiSach";
            this.cbELoaiSach.Size = new System.Drawing.Size(185, 28);
            this.cbELoaiSach.TabIndex = 61;
            // 
            // lbMaSach
            // 
            this.lbMaSach.AutoSize = true;
            this.lbMaSach.Location = new System.Drawing.Point(35, 29);
            this.lbMaSach.Name = "lbMaSach";
            this.lbMaSach.Size = new System.Drawing.Size(72, 20);
            this.lbMaSach.TabIndex = 63;
            this.lbMaSach.Text = "Mã Sách: ";
            // 
            // lbMaSachValue
            // 
            this.lbMaSachValue.AutoSize = true;
            this.lbMaSachValue.Location = new System.Drawing.Point(102, 29);
            this.lbMaSachValue.Name = "lbMaSachValue";
            this.lbMaSachValue.Size = new System.Drawing.Size(17, 20);
            this.lbMaSachValue.TabIndex = 65;
            this.lbMaSachValue.Text = "0";
            // 
            // QuanLySachEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 672);
            this.Controls.Add(this.lbMaSachValue);
            this.Controls.Add(this.lbMaSach);
            this.Controls.Add(this.cbETenTacGia);
            this.Controls.Add(this.cbELoaiSach);
            this.Controls.Add(this.rbEChuaTra);
            this.Controls.Add(this.rbEDaTra);
            this.Controls.Add(this.btnELuu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbETenSach);
            this.Name = "QuanLySachEdit";
            this.Text = "QuanLySachEdit";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbETenSach;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnELuu;
        private System.Windows.Forms.RadioButton rbEDaTra;
        private System.Windows.Forms.RadioButton rbEChuaTra;
        private System.Windows.Forms.ComboBox cbETenTacGia;
        private System.Windows.Forms.ComboBox cbELoaiSach;
        private System.Windows.Forms.Label lbMaSach;
        private System.Windows.Forms.Label lbMaSachValue;
    }
}