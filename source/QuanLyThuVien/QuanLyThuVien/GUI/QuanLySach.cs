﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using QuanLyThuVien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.GUI
{
    public partial class QuanLySach : Form
    {

        quanlythuvienContext context;
        public QuanLySach()
        {
            InitializeComponent();
            context = new quanlythuvienContext();
            loadData();
        }


        #region methods
        void search()
        {
            dgvSach.Columns.Clear();
            var conf = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", true, true)
               .Build();

            SqlConnection connection = new SqlConnection(conf.GetConnectionString("DBconnection"));

            string query = "select a.MaSach,a.TenSach,b.TenLoaiSach,c.TenTacGia,a.TrangThai,a.MaLoaiSach,a.MaTacGia " +
                "from Sach as a, LoaiSach as b, TacGia as c " +
                $"where a.MaLoaiSach = b.MaLoaiSach and a.MaTacGia = c.MaTacGia and a.TenSach like '%{txbTenSach.Text}%' " +
                $"and b.TenLoaiSach like '%{txbLoaiSach.Text}%' and c.TenTacGia like '%{txbTenTacGia.Text}%'";

            SqlCommand command = new SqlCommand(query, connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            dgvSach.DataSource = table;
            int count = dgvSach.Columns.Count;

            DataGridViewButtonColumn btnEdit = new DataGridViewButtonColumn
            {
                Name = "",
                Text = "Chỉnh sửa",
                UseColumnTextForButtonValue = true
            };

            dgvSach.Columns.Insert(count, btnEdit);

            dgvSach.Columns["MaLoaiSach"].Visible = false;
            dgvSach.Columns["MaTacGia"].Visible = false;

            lbNum.Text = $"{dgvSach.Rows.Count - 1}";

        }

        void loadData()
        {
            dgvSach.Columns.Clear();
            var listSach = from s in context.Saches
                           select new
                           {
                               MaSach = s.MaSach,
                               TenSach = s.TenSach,
                               TenLoaiSach = s.MaLoaiSachNavigation.TenLoaiSach,
                               TenTacGia = s.MaTacGiaNavigation.TenTacGia,
                               TrangThai = s.TrangThai,
                               MaLoaiSach = s.MaLoaiSach,
                               MaTacGia = s.MaTacGia
                           };

            dgvSach.DataSource = listSach.ToList();

            int count = dgvSach.Columns.Count;

            DataGridViewButtonColumn btnEdit = new DataGridViewButtonColumn
            {
                Name = "Edit",
                Text = "Chỉnh sửa",
                UseColumnTextForButtonValue = true
            };

            dgvSach.Columns.Insert(count, btnEdit);

            dgvSach.Columns["TrangThai"].ReadOnly = true;
            dgvSach.Columns["MaLoaiSach"].Visible = false;
            dgvSach.Columns["MaTacGia"].Visible = false;

            lbNum.Text = $"{dgvSach.Rows.Count - 1}";

        }

        #endregion

        #region events

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void dgvSach_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvSach.Columns["Edit"].Index)
            {
                int maSach;
                maSach = (int)dgvSach.Rows[e.RowIndex].Cells["MaSach"].Value;

                QuanLySachEdit f = new QuanLySachEdit(maSach);
                this.Hide();
                f.Show();
                /*DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                    loadData();*/
            }
            #endregion
        }

        private void btnThemSach_Click(object sender, EventArgs e)
        {
            QuanLySachAdd f = new QuanLySachAdd();
            this.Hide();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main f = new Main();
            this.Hide();
            f.Show();
        }
    }
}