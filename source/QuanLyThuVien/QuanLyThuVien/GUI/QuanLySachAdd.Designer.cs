﻿namespace QuanLyThuVien.GUI
{
    partial class QuanLySachAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnELuu = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txbALoaiSachInput = new System.Windows.Forms.TextBox();
            this.txbATenSach = new System.Windows.Forms.TextBox();
            this.cbALoaiSach = new System.Windows.Forms.ComboBox();
            this.cbATenTacGia = new System.Windows.Forms.ComboBox();
            this.txbANum = new System.Windows.Forms.NumericUpDown();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.lbNgaySinh = new System.Windows.Forms.Label();
            this.lbGioiTinh = new System.Windows.Forms.Label();
            this.lbDiaChi = new System.Windows.Forms.Label();
            this.txbDiaChi = new System.Windows.Forms.TextBox();
            this.rbNam = new System.Windows.Forms.RadioButton();
            this.rbNu = new System.Windows.Forms.RadioButton();
            this.lbSDT = new System.Windows.Forms.Label();
            this.txbSDT = new System.Windows.Forms.TextBox();
            this.lbEmail = new System.Windows.Forms.Label();
            this.txbEmail = new System.Windows.Forms.TextBox();
            this.lbOp1 = new System.Windows.Forms.Label();
            this.lbOp3 = new System.Windows.Forms.Label();
            this.lbOp2 = new System.Windows.Forms.Label();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.txbATenTacGiaInput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txbANum)).BeginInit();
            this.SuspendLayout();
            // 
            // btnELuu
            // 
            this.btnELuu.Location = new System.Drawing.Point(287, 328);
            this.btnELuu.Name = "btnELuu";
            this.btnELuu.Size = new System.Drawing.Size(94, 29);
            this.btnELuu.TabIndex = 19;
            this.btnELuu.Text = "Lưu";
            this.btnELuu.UseVisualStyleBackColor = true;
            this.btnELuu.Click += new System.EventHandler(this.btnELuu_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(196, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Loại Sách";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Tên Tác Giả";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(525, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 20);
            this.label2.TabIndex = 16;
            this.label2.Text = "Số Lượng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(196, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Tên Sách";
            // 
            // txbALoaiSachInput
            // 
            this.txbALoaiSachInput.Location = new System.Drawing.Point(621, 143);
            this.txbALoaiSachInput.Name = "txbALoaiSachInput";
            this.txbALoaiSachInput.Size = new System.Drawing.Size(185, 27);
            this.txbALoaiSachInput.TabIndex = 13;
            // 
            // txbATenSach
            // 
            this.txbATenSach.Location = new System.Drawing.Point(287, 61);
            this.txbATenSach.Name = "txbATenSach";
            this.txbATenSach.Size = new System.Drawing.Size(185, 27);
            this.txbATenSach.TabIndex = 12;
            // 
            // cbALoaiSach
            // 
            this.cbALoaiSach.FormattingEnabled = true;
            this.cbALoaiSach.Location = new System.Drawing.Point(287, 142);
            this.cbALoaiSach.Name = "cbALoaiSach";
            this.cbALoaiSach.Size = new System.Drawing.Size(185, 28);
            this.cbALoaiSach.TabIndex = 22;
            // 
            // cbATenTacGia
            // 
            this.cbATenTacGia.FormattingEnabled = true;
            this.cbATenTacGia.Location = new System.Drawing.Point(287, 224);
            this.cbATenTacGia.Name = "cbATenTacGia";
            this.cbATenTacGia.Size = new System.Drawing.Size(185, 28);
            this.cbATenTacGia.TabIndex = 23;
            // 
            // txbANum
            // 
            this.txbANum.Location = new System.Drawing.Point(621, 59);
            this.txbANum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txbANum.Name = "txbANum";
            this.txbANum.Size = new System.Drawing.Size(82, 27);
            this.txbANum.TabIndex = 24;
            this.txbANum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(529, 145);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(63, 20);
            this.linkLabel1.TabIndex = 25;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Chưa có";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(529, 232);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(63, 20);
            this.linkLabel2.TabIndex = 26;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Chưa có";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // lbNgaySinh
            // 
            this.lbNgaySinh.AutoSize = true;
            this.lbNgaySinh.Location = new System.Drawing.Point(514, 298);
            this.lbNgaySinh.Name = "lbNgaySinh";
            this.lbNgaySinh.Size = new System.Drawing.Size(76, 20);
            this.lbNgaySinh.TabIndex = 27;
            this.lbNgaySinh.Text = "Ngày Sinh";
            // 
            // lbGioiTinh
            // 
            this.lbGioiTinh.AutoSize = true;
            this.lbGioiTinh.Location = new System.Drawing.Point(522, 371);
            this.lbGioiTinh.Name = "lbGioiTinh";
            this.lbGioiTinh.Size = new System.Drawing.Size(68, 20);
            this.lbGioiTinh.TabIndex = 29;
            this.lbGioiTinh.Text = "Giới Tính";
            // 
            // lbDiaChi
            // 
            this.lbDiaChi.AutoSize = true;
            this.lbDiaChi.Location = new System.Drawing.Point(533, 426);
            this.lbDiaChi.Name = "lbDiaChi";
            this.lbDiaChi.Size = new System.Drawing.Size(57, 20);
            this.lbDiaChi.TabIndex = 31;
            this.lbDiaChi.Text = "Địa Chỉ";
            // 
            // txbDiaChi
            // 
            this.txbDiaChi.Location = new System.Drawing.Point(621, 423);
            this.txbDiaChi.Name = "txbDiaChi";
            this.txbDiaChi.Size = new System.Drawing.Size(185, 27);
            this.txbDiaChi.TabIndex = 30;
            // 
            // rbNam
            // 
            this.rbNam.AutoSize = true;
            this.rbNam.Location = new System.Drawing.Point(637, 369);
            this.rbNam.Name = "rbNam";
            this.rbNam.Size = new System.Drawing.Size(62, 24);
            this.rbNam.TabIndex = 32;
            this.rbNam.TabStop = true;
            this.rbNam.Text = "Nam";
            this.rbNam.UseVisualStyleBackColor = true;
            // 
            // rbNu
            // 
            this.rbNu.AutoSize = true;
            this.rbNu.Location = new System.Drawing.Point(740, 367);
            this.rbNu.Name = "rbNu";
            this.rbNu.Size = new System.Drawing.Size(50, 24);
            this.rbNu.TabIndex = 33;
            this.rbNu.TabStop = true;
            this.rbNu.Text = "Nữ";
            this.rbNu.UseVisualStyleBackColor = true;
            // 
            // lbSDT
            // 
            this.lbSDT.AutoSize = true;
            this.lbSDT.Location = new System.Drawing.Point(555, 487);
            this.lbSDT.Name = "lbSDT";
            this.lbSDT.Size = new System.Drawing.Size(35, 20);
            this.lbSDT.TabIndex = 35;
            this.lbSDT.Text = "SDT";
            // 
            // txbSDT
            // 
            this.txbSDT.Location = new System.Drawing.Point(621, 484);
            this.txbSDT.Name = "txbSDT";
            this.txbSDT.Size = new System.Drawing.Size(185, 27);
            this.txbSDT.TabIndex = 34;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(544, 552);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(46, 20);
            this.lbEmail.TabIndex = 37;
            this.lbEmail.Text = "Email";
            // 
            // txbEmail
            // 
            this.txbEmail.Location = new System.Drawing.Point(621, 549);
            this.txbEmail.Name = "txbEmail";
            this.txbEmail.Size = new System.Drawing.Size(185, 27);
            this.txbEmail.TabIndex = 36;
            // 
            // lbOp1
            // 
            this.lbOp1.AutoSize = true;
            this.lbOp1.Location = new System.Drawing.Point(834, 426);
            this.lbOp1.Name = "lbOp1";
            this.lbOp1.Size = new System.Drawing.Size(75, 20);
            this.lbOp1.TabIndex = 38;
            this.lbOp1.Text = "(tùy chọn)";
            // 
            // lbOp3
            // 
            this.lbOp3.AutoSize = true;
            this.lbOp3.Location = new System.Drawing.Point(834, 556);
            this.lbOp3.Name = "lbOp3";
            this.lbOp3.Size = new System.Drawing.Size(75, 20);
            this.lbOp3.TabIndex = 39;
            this.lbOp3.Text = "(tùy chọn)";
            // 
            // lbOp2
            // 
            this.lbOp2.AutoSize = true;
            this.lbOp2.Location = new System.Drawing.Point(834, 491);
            this.lbOp2.Name = "lbOp2";
            this.lbOp2.Size = new System.Drawing.Size(75, 20);
            this.lbOp2.TabIndex = 40;
            this.lbOp2.Text = "(tùy chọn)";
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.Location = new System.Drawing.Point(621, 298);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(185, 27);
            this.dtpNgaySinh.TabIndex = 41;
            // 
            // txbATenTacGiaInput
            // 
            this.txbATenTacGiaInput.Location = new System.Drawing.Point(621, 232);
            this.txbATenTacGiaInput.Name = "txbATenTacGiaInput";
            this.txbATenTacGiaInput.Size = new System.Drawing.Size(185, 27);
            this.txbATenTacGiaInput.TabIndex = 42;
            // 
            // QuanLySachAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 603);
            this.Controls.Add(this.txbATenTacGiaInput);
            this.Controls.Add(this.dtpNgaySinh);
            this.Controls.Add(this.lbOp2);
            this.Controls.Add(this.lbOp3);
            this.Controls.Add(this.lbOp1);
            this.Controls.Add(this.lbEmail);
            this.Controls.Add(this.txbEmail);
            this.Controls.Add(this.lbSDT);
            this.Controls.Add(this.txbSDT);
            this.Controls.Add(this.rbNu);
            this.Controls.Add(this.rbNam);
            this.Controls.Add(this.lbDiaChi);
            this.Controls.Add(this.txbDiaChi);
            this.Controls.Add(this.lbGioiTinh);
            this.Controls.Add(this.lbNgaySinh);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.txbANum);
            this.Controls.Add(this.cbATenTacGia);
            this.Controls.Add(this.cbALoaiSach);
            this.Controls.Add(this.btnELuu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbALoaiSachInput);
            this.Controls.Add(this.txbATenSach);
            this.Name = "QuanLySachAdd";
            this.Text = "Thêm Sách";
            ((System.ComponentModel.ISupportInitialize)(this.txbANum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnELuu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbALoaiSachInput;
        private System.Windows.Forms.TextBox txbATenSach;
        private System.Windows.Forms.ComboBox cbALoaiSach;
        private System.Windows.Forms.ComboBox cbATenTacGia;
        private System.Windows.Forms.NumericUpDown txbANum;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label lbNgaySinh;
        private System.Windows.Forms.Label lbGioiTinh;
        private System.Windows.Forms.Label lbDiaChi;
        private System.Windows.Forms.TextBox txbDiaChi;
        private System.Windows.Forms.RadioButton rbNam;
        private System.Windows.Forms.RadioButton rbNu;
        private System.Windows.Forms.Label lbSDT;
        private System.Windows.Forms.TextBox txbSDT;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.TextBox txbEmail;
        private System.Windows.Forms.Label lbOp1;
        private System.Windows.Forms.Label lbOp3;
        private System.Windows.Forms.Label lbOp2;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.TextBox txbATenTacGiaInput;
    }
}