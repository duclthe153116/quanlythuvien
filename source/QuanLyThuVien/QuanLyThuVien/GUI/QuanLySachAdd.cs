﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using QuanLyThuVien.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyThuVien.GUI
{
    public partial class QuanLySachAdd : Form
    {
        quanlythuvienContext context;

        public QuanLySachAdd()
        {
            InitializeComponent();
            context = new quanlythuvienContext();

            txbALoaiSachInput.Visible = false;
            txbATenTacGiaInput.Visible = false;
            dtpNgaySinh.Visible = false;
            txbDiaChi.Visible = false;
            txbEmail.Visible = false;
            txbSDT.Visible = false;
            rbNam.Visible = false;
            rbNu.Visible = false;

            lbDiaChi.Visible = false;
            lbEmail.Visible = false;
            lbGioiTinh.Visible = false;
            lbNgaySinh.Visible = false;
            lbOp1.Visible = false;
            lbOp2.Visible = false;
            lbOp3.Visible = false;
            lbSDT.Visible = false;

            loadLoaiSach();
            loadTacGia();
        }

        #region methods

        void loadLoaiSach()
        {
            var conf = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", true, true)
               .Build();

            SqlConnection connection = new SqlConnection(conf.GetConnectionString("DBconnection"));

            string query = "select TenLoaiSach from LoaiSach";

            SqlCommand command = new SqlCommand(query, connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            cbALoaiSach.DisplayMember = "TenLoaiSach";
            cbALoaiSach.ValueMember = "TenLoaiSach";
            cbALoaiSach.DataSource = table;

        }

        void loadTacGia()
        {
            var conf = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", true, true)
               .Build();

            SqlConnection connection = new SqlConnection(conf.GetConnectionString("DBconnection"));

            string query = "select TenTacGia from TacGia";

            SqlCommand command = new SqlCommand(query, connection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);

            cbATenTacGia.DisplayMember = "TenTacGia";
            cbATenTacGia.ValueMember = "TenTacGia";
            cbATenTacGia.DataSource = table;

        }

        #endregion

        #region events

        #endregion

        int countClickLS = 0;
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            countClickLS++;
            if (countClickLS % 2 == 0)
            {
                txbALoaiSachInput.Visible = false;
                txbALoaiSachInput.Text = "";
                cbALoaiSach.Enabled = true;
            }
            else
            {
                txbALoaiSachInput.Visible = true;
                cbALoaiSach.Enabled = false;
            }
        }

        int countClickTG = 0;
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            countClickTG++;
            if (countClickTG % 2 == 0)
            {
                txbATenTacGiaInput.Visible = false;
                dtpNgaySinh.Visible = false;
                txbDiaChi.Visible = false;
                txbEmail.Visible = false;
                txbSDT.Visible = false;
                rbNam.Visible = false;
                rbNu.Visible = false;

                lbDiaChi.Visible = false;
                lbEmail.Visible = false;
                lbGioiTinh.Visible = false;
                lbNgaySinh.Visible = false;
                lbOp1.Visible = false;
                lbOp2.Visible = false;
                lbOp3.Visible = false;
                lbSDT.Visible = false;

                txbATenTacGiaInput.Text = "";
                cbATenTacGia.Enabled = true;
            }
            else
            {
                txbATenTacGiaInput.Visible = true;
                dtpNgaySinh.Visible = true;
                txbDiaChi.Visible = true;
                txbEmail.Visible = true;
                txbSDT.Visible = true;
                rbNam.Visible = true;
                rbNu.Visible = true;

                lbDiaChi.Visible = true;
                lbEmail.Visible = true;
                lbGioiTinh.Visible = true;
                lbNgaySinh.Visible = true;
                lbOp1.Visible = true;
                lbOp2.Visible = true;
                lbOp3.Visible = true;
                lbSDT.Visible = true;

                cbATenTacGia.Enabled = false;
            }
        }

        private void btnELuu_Click(object sender, EventArgs e)
        {

            if (txbATenSach.Text == "")
            {
                MessageBox.Show("Vui lòng nhập tên sách!");
                return;
            }

            if (countClickLS % 2 != 0)
            {
                if (txbALoaiSachInput.Text == "")
                {
                    MessageBox.Show("Vui lòng chọn loại sách!");
                    return;
                }
            }

            if (countClickTG % 2 != 0)
            {
                if (txbATenTacGiaInput.Text == "")
                {
                    MessageBox.Show("Vui lòng điền tên tác giả!");
                    return;
                }

                if (rbNam.Checked == false && rbNu.Checked == false)
                {
                    MessageBox.Show("Vui lòng chọn giới tính!");
                    return;
                }
            }

            //Insert into database
            int LoaiSachID = -1;
            int TacGiaID = -1;

            //insert into LoaiSach table
            if (countClickLS % 2 != 0)
            {
                context.LoaiSaches.Add(new LoaiSach { TenLoaiSach = txbALoaiSachInput.Text });
                context.SaveChanges();

                List<LoaiSach> ls = context.LoaiSaches.ToList();
                LoaiSachID = ls[ls.Count() - 1].MaLoaiSach;
            }
            else
            {
                var k = from l in context.LoaiSaches
                        where l.TenLoaiSach == cbALoaiSach.Text
                        select l.MaLoaiSach;
                List<int> t = k.ToList();
                LoaiSachID = t[t.Count - 1];
            }

            //insert into TacGia table
            if (countClickTG % 2 != 0)
            {
                bool sex = false;
                if (rbNam.Checked == true)
                {
                    sex = true;
                }

                context.TacGia.Add(new TacGium
                {
                    TenTacGia = txbATenTacGiaInput.Text,
                    NgaySinh = dtpNgaySinh.Value,
                    GioiTinh = sex,
                    DiaChi = txbDiaChi.Text,
                    SoDienThoai = txbSDT.Text,
                    Email = txbEmail.Text
                });

                context.SaveChanges();

                List<TacGium> ls = context.TacGia.ToList();
                TacGiaID = ls[ls.Count() - 1].MaTacGia;
            }
            else
            {
                var k = from l in context.TacGia
                        where l.TenTacGia == cbATenTacGia.Text
                        select l.MaTacGia;
                List<int> t = k.ToList();
                TacGiaID = t[t.Count - 1];
            }

            for (int i = 0; i < txbANum.Value; i++)
            {
                context.Add(new Sach { MaLoaiSach = LoaiSachID, MaTacGia = TacGiaID, TenSach = txbATenSach.Text, TrangThai = false });
                context.SaveChanges();
            }

            MessageBox.Show("Thêm sách thành công!");

            QuanLySach f = new QuanLySach();
            this.Hide();
            f.Show();
        }
    }
}